# README
[TOC]
## 下载
`git clone git@bitbucket.org:xueteng/smart.git`

## 配置
`npm install`

`bower install`

## 运行
`grunt server`


## 文档结构

### bower_components
> bower安装的模块文件夹

- angular (angularJS核心模块)
- angular-router (angularJS单页面路由管理模块)
- angular-ui-bootstrap-bower (bootstrap-angularJS模块)
- bootstrap (样式)
- cometd-jquery (未知-未使用)
- Cumulocity-client-javascript (c8y模块核心)
- d3 (d3.js-未使用)
- highcharts-ng (highcharts模块的angular指令，已弃用)
- jquery (jquery库)
- lodash (lodash库)
- moment (moment库)
- ng-dialog (angularJS模态对话框模块)
- ng-file-upload (angularJS文件上传模块)

### css
> 样式

- dashboard (主页样式)
- login (登录页样式)
- map (map模块样式)

### img
> 图片

### js
> 项目js

- alarms_ctrl.js(地图下方警报组件的controller)
- charts_ctrl.js(地图右侧图表的controller-已弃用，整体并入map_ctrl.js)
- login_ctrl.js(登录页的controller)
- main_ctrl.js(主页的controller)
- map_ctrl.js(地图组件的controller)
- t1.js (设备列表页和报警列表页的controller)
- t3_ctrl.js (规则编辑页的controller)
- app.js (app入口js，定义项目名、依赖、路由配置、c8y项目url和key)
- public_data_service.js (共享数据服务模块)
- section_dir.js (eg-section指令-已弃用)
- treeview_ctrl.js (treeview组件的controller-已弃用treeview)

### lib

> 未使用bower或者npm进行管理的模块

- treeview模块，已弃用

### node_modules
> npm安装的模块文件夹

- echarts (echarts图表模块)
- echarts-ng (echarts模块的angular指令)
- grunt (grunt模块，此项目中仅用于提供grunt-http-server的服务)
- grunt-http-server(grunt-http-server模块，用于创建小型http服务器)
- http-server(http-server模块)
- zrender(canvas绘图模块，echarts的依赖)

### sections
> eg-section模块使用的view，已弃用

### views
> 项目的views，以下几项已弃用：
charts.html,
datapoint.html,
devicechart.html,
mainpage.html,
section.html

- login.html (登录页)
- main.html (首页，含地图组件、报警列表组件，统计信息组件)
- t1.html (报警列表)
- t2.html (设备列表)
- t3.html (规则列表)

### .gitignore
> git忽略的文件

### bower.json
> bower模块管理的配置文件

### cumulocity.json
> cumulocity项目的配置文件

### gruntfiles.js
> grunt模块的入口，用于启动http

### index.html
> 项目目录doc

### main.html
> 主页的view，已弃用


### package.json
> npm模块管理的配置文件


### README
> README文件
