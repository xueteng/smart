(function() {
    'use strict';

    angular.module('basic').controller('mapCtrl', [
        '$scope',
        '$http',
        '$interval',
        '$echarts',
        '$timeout',
        '$location',
        'c8yBase',
        'c8yMeasurements',
        'c8yDevices',
        'c8yDeviceControl',
        'c8yInventory',
        'PublicData',
        mapCtrl
    ]);
    /**
     * [mapCtrl map页controller]
     * @param  {object} $scope           处理作用域
     * @param  {object} $http            封装http请求
     * @param  {object} $interval        封装interval
     * @param  {object} $echarts         封装echarts
     * @param  {object} $timeout         封装timeout
     * @param  {object} $location        封装location
     * @param  {object} c8yBase          c8y基本依赖
     * @param  {object} c8yMeasurements  c8y测量值模块
     * @param  {object} c8yDevices       c8y设备模块
     * @param  {object} c8yDeviceControl c8y设备控制模块
     * @param  {object} c8yInventory     c8y对象管理模块
     * @param  {object} PublicData       controller之间共享数据的service
     * @return {[type]}                  [description]
     */
    function mapCtrl(
        $scope,
        $http,
        $interval,
        $echarts,
        $timeout,
        $location,
        c8yBase,
        c8yMeasurements,
        c8yDevices,
        c8yDeviceControl,
        c8yInventory,
        PublicData
    ) {
        //地图信息基本配置
        $scope.locationData = {
            //mapSize分5档：1全国2省3市4地区5街道
            mapSize:1,
            name:"全国",
            center:[104.84, 36.65],
            groups:[],
            zooms:[4,12,13,14,15],
            operations:["一般规则","雾霾天规则","常亮规则","演示规则","当前无规则"]
        };
        //预设值
        $scope.testData = [
            {
                name:"全国",
                zoom:"4",
                center:[104.84, 36.65]
            }
        ];
        //地图参数配置
        $scope.mapInfo = {
            resizeEnable: true,
            zoom: 11,
            center: [116.389485, 39.930639],
            zooms: [3,19],
            expandZoomRange: true
        };
        //用于展示的信息
        $scope.checkingResults = {
            originalData : {
                UNAVAILABLE : 0,
                deviceCount : 0,
                AVAILABLE :0,
                testDevice :0
            },
            data : {}
        }
        //marker的容器
        $scope.markers = [];
        //群组标记的容器
        $scope.circles = [];
        //测量值的容器
        $scope.devicesMeasurements = [];
        //设备或群组信息的容器
        $scope.groupsOrDevicesInfo = [];
        //当前展示物的容器
        $scope.currentData = [];
        //当前展示物的测量值的容器
        $scope.currentMeasurements = []
        //生成echarts实例并获取id
        $scope.DISTRIBUTION_ID = $echarts.generateInstanceIdentity();
        //echarts的配置
        $scope.distribution = {
            identity: $echarts.generateInstanceIdentity(),
            dimension: '16:9',
            label: {
                normal: {
                    textStyle: {
                        color: 'rgba(255, 255, 255, 0.8)'
                    }
                }
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            labelLine: {
                normal: {
                    lineStyle: {
                        color: 'rgba(255, 255, 255, 0.3)'
                    },
                    smooth: 0.2,
                    length: 10,
                    length2: 20
                }
            },
            series: [{
                name: '功耗（单位：W）',
                type: 'pie',
                data: [
                    {value:0,name:"北京"},
                    {value:0,name:"广州"}
                ],
                // roseType:'radius'
            }],
            color:['green', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3'],
            itemStyle: {
                normal: {
                    // color: '#c23531',
                    shadowBlur: 200,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        };

        /**
         * 根据当前展示物调整$scope.groupsOrDevicesInfo的值
         * @param  {string} data 当前展示物的id
         * @return {[type]}      [description]
         */
        $scope.getGroupOrDevicesData = function(data){
            console.log("开始加载群组/设备信息");
            if($scope.locationData.mapSize==5){
                console.log("展示devices信息")
                getDevices(data);
            }else if ($scope.locationData.mapSize==1) {
                console.log("展示最高级别群组")
                getRootGroups();
                // getMeasurements($scope.currentData[$scope.locationData.mapSize].id)
            }else {
                if(data){
                    console.log("地图尺寸调整")
                    console.log("展示群组")
                    getGroups(data.id);
                    getMeasurements(data.id);
                }else {
                    console.log("自动刷新当前级别视图");
                    getGroups($scope.currentData[$scope.locationData.mapSize-1]);
                    getMeasurements($scope.currentData[$scope.locationData.mapSize-1]);
                }
            }

        };
        /**
         * 刷新markers，清空地图标记、增加新的标记，为标记绑定事件
         * @return {[type]} [description]
         */
        $scope.refreshMarkers = function(){
            $scope.map.clearMap();
            angular.forEach($scope.devicesMeasurements, function(item, index) {
              if(item.status.status=="AVAILABLE"){
                $scope.markers[index] = new AMap.Marker({
                  map:$scope.map,
                  icon: "img/LED.png",
                  position: [item.lng, item.lat]
                });
              }else {
                $scope.markers[index] = new AMap.Marker({
                  map:$scope.map,
                  icon: "img/errorLED.png",
                  position: [item.lng, item.lat]
                });
              }
                $scope.markers[index].setLabel({
                    offset: new AMap.Pixel(20, 20), //修改label相对于maker的位置
                    content: item.name
                });
                $scope.markers[index].on('click', function() {
                    PublicData.saveInfo("deviceId4SingleCtrl",item.id);
                    // goTo("/#/t2")
                    window.location.href="#/t2";
                    // $location.path("/t2")

                });
            })
        }
        /**
         * 更新群组标记
         * @return {[type]} [description]
         */
        $scope.refreshCircles = function(){
            $scope.map.clearMap();
            $scope.markers = [];//清空markers确保自动定位的效果
            angular.forEach($scope.groupsOrDevicesInfo, function(item, index,data) {
                $scope.markers[index] = new AMap.Marker({
                    map:$scope.map,
                    icon: "img/mark_r.png",
                    position: [item.lng, item.lat],
                    extData:{
                        id:item.id,
                        center:[item.lng, item.lat],
                        name:item.name,
                        currentOperation:item.com_gdlk_smartLED_PWMcontrol_setting,
                        currentOperationSetting:item.com_gdlk_smartLED_PWMcontrol_setting.Mode
                    }
                });
                $scope.markers[index].setLabel({
                    offset: new AMap.Pixel(20, 20), //修改label相对于maker的位置
                    content: item.name
                });
                $scope.markers[index].on('click', function(e) {
                  c8yInventory.childAssets(e.target.G.extData.id).then(function(data){
                    console.log(data);
                    if(data.length>0){
                      //放大地图
                      $scope.locationData.mapSize++;
                      console.log($scope.locationData)
                      //根据标记属性获取值
                      $scope.getGroupOrDevicesData(e.target.G.extData);
                      $scope.currentData[$scope.locationData.mapSize-1] = e.target.G.extData;
                      $scope.checkingResults.data = angular.copy($scope.checkingResults.originalData);
                      $scope.check($scope.currentData[$scope.locationData.mapSize-1].id)

                    }else {
                      alert("该群组无子群组")
                    }
                  })
                });
                if(index == data.length-1){
                  console.log($scope.circles);
                  $scope.map.setFitView($scope.markers)
                }
            })
            /**
             * 真正的circles方法，已弃置，用于在地图上用圆形标记群组范围
             */
            // angular.forEach($scope.groupsOrDevicesInfo, function(item, index) {
            //     $scope.circles[index] = new AMap.Circle({
            //         map:$scope.map,
            //         radius:[300000,30000,3000,300][$scope.locationData.mapSize-1],
            //         strokeColor: "#faf",
            //         center: [item.lng, item.lat],
            //         fillColor:"#afa",
            //         strokeOpacity:0.5,
            //         fillOpacity:0.5,
            //         extData:{
            //             id:item.id,
            //             center:[item.lng, item.lat]
            //         }
            //     });
            //     // $scope.circles[index].setLabel({
            //     //     offset: new AMap.Pixel(20, 20), //修改label相对于maker的位置
            //     //     content: item.name
            //     // });
            //     $scope.circles[index].on('click', function(e) {
            //         $scope.locationData.mapSize++;
            //         console.log($scope.locationData)
            //         $scope.getGroupOrDevicesData(e.target.G.extData);
            //         $scope.currentData = e.target.G.extData;
            //     });
            //     // console.log($scope.circles)
            // })
        }
        /**
         * Amap实例创建，参数配置
         * @return {[type]} [description]
         */
        $scope.initAMap = function() {
            $scope.map = new AMap.Map('amap-container', $scope.mapInfo);
            // $scope.map.on('click', function(e) {
            //     alert('您在[ ' + e.lnglat.getLng() + ',' + e.lnglat.getLat() + ' ]的位置点击了地图！');
            // });
            $scope.map.setMapStyle("light");
            // $scope.map.plugin(['control/BasicControl'],function(){
            //     //加载工具条
            //     map.addControl(new BasicControl.Zoom({
            //     position: 'lt', //left top，左上角
            //     showZoomNum: true //显示zoom值
            //     }));
            // });
            // $scope.map.on('zoomend', function(){
            //     console.log($scope.map.getZoom());
            //     var currentZoom = $scope.map.getZoom();
            //     $timeout(function () {
            //         if(currentZoom<=9){
            //             $scope.locationData.mapSize = 1;
            //             $scope.getGroupOrDevicesData()
            //         }else if(currentZoom<=11){
            //             $scope.locationData.mapSize = 2;
            //             console.log($scope.currentData)
            //             $scope.getGroupOrDevicesData($scope.currentData[$scope.locationData.mapSize])
            //         }
            //     }, 1000);
            // })
        };
        /**
         * 规则编辑按钮的触发事件
         * @param  {string} id 当前群组的id
         * @return {[type]}    [description]
         */
        $scope.editOperation = function(id){
            PublicData.saveInfo("deviceId4OperationEdit",id);
            PublicData.saveInfo("currentMapInfo",{
                currentData:$scope.currentData,
                mapSize:$scope.locationData.mapSize
            });
            // goTo("/#/t3");
            $location.path("/t3")

        }
        /**
         * 检查设备状态的方法，会递归调用countDevices方法计算设备数
         * @param  {string} id 根对象的id
         * @return {[type]}    [description]
         */
        $scope.check = function(id){
          console.log($scope.currentData);
            $scope.checkingResults.data = angular.copy($scope.checkingResults.originalData);
            $scope.checkingResults.data.show = true;
            if(id){
                c8yInventory.childAssets(id).then(function(data){
                    countDevices(data);
                })
            }else{
                // console.log($scope.groupsOrDevicesInfo);
                angular.forEach($scope.groupsOrDevicesInfo,function(item){
                    c8yInventory.childAssets(item.id).then(function(data){
                        countDevices(data);
                    })
                })
            }
        }
        /**
         * 返回上一级
         * @return {[type]} [description]
         */
        $scope.backup =function(){
            if($scope.locationData.mapSize>=2){
                $scope.locationData.mapSize--;
                $scope.getGroupOrDevicesData();
                if($scope.locationData.mapSize>1){
                  $scope.check($scope.currentData[$scope.locationData.mapSize-1].id)
                }else {
                  //else情况已在getRootGroup方法中执行check
                  // $scope.check();
                }
            }
        }
        /**
         * 获取已存储的数据并更新地图内容
         * @return {[type]} [description]
         */
        $scope.getStoredData = function(){
            var temp = PublicData.getInfo("currentMapInfo");
            var index = PublicData.getInfo("savedIndex");
            if(temp){
              console.log(temp);
                $scope.locationData.mapSize = temp.mapSize;
                $scope.currentData = temp.currentData;
                if($scope.locationData.mapSize>1){
                  $scope.check($scope.currentData[$scope.locationData.mapSize-1].id)
                }
                PublicData.delInfo("currentMapInfo")
            }
            if(index){
              $scope.currentData[$scope.locationData.mapSize-1].currentOperationSetting = index;
              // $scope.currentData.
            }
        }
        /**
         * 递归获取设备状态并计数
         * @param  {array} obj  当前设备/群组列表
         * @return {}           修改了$scope.checkingResults的值，无返回值
         */
        function countDevices(obj){
            if(obj.length>0){
                angular.forEach(obj,function(item,index){
                    if(item.c8y_IsDevice){
                        $scope.checkingResults.data.deviceCount++;
                        if(item.c8y_Availability){
                            if(item.c8y_Availability.status=="UNAVAILABLE"){
                                $scope.checkingResults.data.UNAVAILABLE++;
                            }else {
                                $scope.checkingResults.data.AVAILABLE++;
                            }
                        }else {
                            $scope.checkingResults.data.testDevice++;
                        }
                    }else {
                        c8yInventory.childAssets(item.id).then(function(data){
                            countDevices(data)
                        })
                    }
                })
            }
        }
        /**
         * 获取根群组信息
         * @return {[type]} 无返回，修改了$scope.groupsOrDevicesInfo,创建了$echarts的实例
         */
        function getRootGroups(){
            c8yInventory.list({"type":"c8y_DeviceGroup"}).then(function(data){
                //重置信息
                $scope.groupsOrDevicesInfo = [];
                $scope.currentMeasurements = [];
                angular.forEach(data,function(item,index){
                    //定位
                    $scope.groupsOrDevicesInfo.push({
                        "lng":Number(item.c8y_Notes&&item.c8y_Notes.split(",")[0]||121.429),
                        "lat":Number(item.c8y_Notes&&item.c8y_Notes.split(",")[1]||31.6527),
                        "id":item.id,
                        "name":item.name,
                        "com_gdlk_smartLED_PWMcontrol_setting":item.com_gdlk_smartLED_PWMcontrol_setting||{Mode:0}
                    })
                    //取值
                    getMeasurements(item.id).then(function(data){
                        $scope.currentMeasurements.push({
                            name:item.name,
                            value:data[0].group.power_consumption.toFixed(2)
                        });
                        $scope.distribution.series[0].data = $scope.currentMeasurements;
                        $echarts.updateEchartsInstance($scope.DISTRIBUTION_ID, $scope.distribution);
                    })
                    //刷新地图和图表
                    if(index == data.length-1) {
                        $scope.refreshCircles();
                        $scope.check();
                    }
                });
            })
        }
        /**
         * 获取群组信息
         * @param  {string} id 当前群组id
         * @return {[type]}    [description]
         */
        function getGroups(id){
            c8yInventory.childAssets(id).then(function(data){
                console.log("groupsInfo");
                console.log(data);
                //重置信息
                $scope.groupsOrDevicesInfo = [];
                $scope.currentMeasurements = [];
                angular.forEach(data,function(item,index){
                    $scope.groupsOrDevicesInfo.push({
                        "lng":Number(item.c8y_Notes&&item.c8y_Notes.split(",")[0]||121.429),
                        "lat":Number(item.c8y_Notes&&item.c8y_Notes.split(",")[1]||31.6527),
                        "id":item.id,
                        "name":item.name,
                        "com_gdlk_smartLED_PWMcontrol_setting":item.com_gdlk_smartLED_PWMcontrol_setting||{Mode:0}
                    })
                    //取值
                    getMeasurements(item.id).then(function(data){
                        console.log(data);
                        $scope.currentMeasurements.push({
                            name:item.name,
                            value:data[0]&&data[0].group.power_consumption.toFixed(2)||0
                        });
                        $scope.distribution.series[0].data = $scope.currentMeasurements;
                        $echarts.updateEchartsInstance($scope.DISTRIBUTION_ID, $scope.distribution);
                    })
                    //刷新地图
                    if(index == data.length-1) {
                        $scope.refreshCircles();
                    }
                });
            })
        }
        /**
         * 获取设备信息
         * @param  {string} id 当前群组id
         * @return {[type]}    [description]
         */
        function getDevices(id){
            //根据group信息获取对应devices，根据devicesId拿取位置信息，根据marks位置自动控制地图缩放及位置
            var tempId;
            if(id){
                tempId=id;
            }else {
                if(PublicData.getInfo("deviceId4OperationEdit")){
                    tempId = PublicData.getInfo("deviceId4OperationEdit")
                }
            }
            c8yInventory.childAssets(tempId).then(function(devices) {
                //重置信息
                $scope.devicesMeasurements = [];
                $scope.currentMeasurements = [];
                angular.forEach(devices,function(item,index){
                    if (item.c8y_Position) {
                        $scope.devicesMeasurements.push({
                            "lng":item.c8y_Position.lng,
                            "lat":item.c8y_Position.lat,
                            "id":item.id,
                            "name":item.name,
                            "status":item.c8y_Availability
                        });
                    }
                    //取值
                    getMeasurements(item.id,"gdlk_Ledlamp_power_consumption_per_hour").then(function(data){
                        console.log(data);
                        $scope.currentMeasurements.push({
                            name:item.name,
                            // value:data[0]&&data[0].power_consumpation&&data[0].power_consumpation.toFixed(2)||0
                            value:data[0]&&data[0].lamp&&data[0].lamp.power_consumption.toFixed(2)||0
                        });
                        $scope.distribution.series[0].data = $scope.currentMeasurements;
                        $echarts.updateEchartsInstance($scope.DISTRIBUTION_ID, $scope.distribution);
                    })
                    //刷新地图
                    if(index == devices.length-1){
                        $scope.refreshMarkers();
                        $scope.map.setFitView($scope.markers)
                    }
                })
            })
        }
        /**
         * 获取测量值
         * @param  {string} id   获取对象的id
         * @param  {string} type 获取对象的type属性的值
         * @return {function}    c8yMeasurements.list方法，返回测量值的集合，本次仅取最新的一个
         */
        function getMeasurements(id,type){
            return c8yMeasurements.list(
                _.assign(c8yBase.timeOrderFilter(), {
                    source: id.id?id.id:id,
                    pageSize:1,
                    revert:true,
                    type:type
                })
            )
        }
        /**
         * 对$location方法的再次封装，测试是否修复跳转问题
         * @param  {string} path 目标地址
         * @return {[type]}      [description]
         */
        function goTo(path){
            // window.location.href = path;
            $location.path(path)
        }
        //页面加载后执行的操作
        PublicData.delInfo("deviceId4SingleCtrl");
        $scope.initAMap();
        $scope.getStoredData();
        $scope.getGroupOrDevicesData();

        $interval(function () {
            $scope.getGroupOrDevicesData();
        }, 60000);
        // $scope.testFns={};
        // $scope.testFns.editInventory = function(){
        //     c8yInventory.save({
        //         id:"12791",
        //         "com_gdlk_smartLED_PWMcontrol_setting":{}
        //     }).then(function(data){
        //         console.log("inventorySaved")
        //         console.log(data)
        //     })
        // }
        // $scope.testFns.editInventory();
    }
})();
