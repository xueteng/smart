(function () {
  'use strict';
  var app = angular.module('basic', [
      'c8y.sdk',
      'c8y.ui',
      'ngRoute',
      'ui.bootstrap',
      'echarts-ng',
      'angularTreeview',
      'ngDialog'
    ]);
  app.config([
    '$routeProvider',
    configRoutes
  ]);
  app.config([
    'c8yCumulocityProvider',
    configQuarkIOE
  ]);

  function configRoutes($routeProvider) {
    $routeProvider
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/:section', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
  }

  function configQuarkIOE(c8yCumulocityProvider) {
    //c8yCumulocityProvider.setAppKey('core-application-key');
    c8yCumulocityProvider.setAppKey('smartled');
    c8yCumulocityProvider.setBaseUrl('https://smart1.quarkioe.com/');
  }

})();
