/**
 * 废除的highcharts模块controller，新版使用echarts并且集成到各个view的controller中
 */
(function() {
  'use strict';

  angular.module('helloCoreApi').controller('ChartsCtrl',[
      '$scope','$interval','$routeParams','c8yDevices','c8yMeasurements','c8yBase','c8yDeviceControl','$echarts',ChartsCtrl]);

  function ChartsCtrl($scope,$interval,$routeParams, c8yDevices, c8yMeasurements,c8yBase, c8yDeviceControl, $echarts) {
      var timezoneOffset = 28800000;
      $scope.DISTRIBUTION_ID = $echarts.generateInstanceIdentity();
      $scope.distribution = {
        identity: $echarts.generateInstanceIdentity(),
        dimension: '16:9',
        xAxis : [
          {
            type : 'category',
            data : ['周一','周二','周三','周四','周五','周六','周日']
          }
        ],
        yAxis : [
          {
            type : 'value'
          }
        ],
        series : [
          {
            name:'联盟广告',
            type:'bar',
            data:[220, 182, 191, 234, 290, 330, 310]
          }
        ]
      };
      $scope.swapShow = 'Power';
      $scope.powerMeasurement = [];
      $scope.currentMeasurement = [];
      $scope.voltageMeasurement = [];


      $scope.highchartsNG = {

          options: {
              chart: {
                  type: 'line',
                  zoomType: 'x',
                  height: 410

              }
          },
          title:{
              text:"选择测量项目"
          },
          xAxis: {
              type: 'datetime',
              dateTimeLabelFormats: {
                 // millisecond: '%H:%M:%S.%L',
                 // second: '%H:%M:%S',
                  minute: '%H:%M',
                  hour: '%H:%M',
                  day: '%m-%d',
                 // week: '%m-%d',
                 // month: '%Y-%m',
                 // year: '%Y'
              }
          },

          yAxis: [{ // Primary yAxis

              title: {
                  text: ''
              },
              max: 200,
              min: 0,

              opposite: true

          }, { // Secondary yAxis
              title: {
                  text: ''
              }
          }],

          series: [{
              type: 'line',
              name: '',
              data: [],
              yAxis: 0

          }, {
              type:'line',
              name: '',
              data: [],
              yAxis:1

          }],

          credits:{
              enabled: false
          },
          legend:{
              enabled: true
          },

          loading: false
      };

      if($routeParams.deviceId != undefined){

          var deviceId = $routeParams.deviceId;
          console.log("show device detail %s",deviceId );

          c8yDevices.detail(deviceId).then(function (res) {
              $scope.device = res.data;
          });

          var updateMeasurement = function() {
               c8yMeasurements.list(
                  _.assign(c8yBase.todayFilter(), {
                      type: 'gdlk_smartLedMeasurement',
                      source: deviceId,
                      revert: true,
                      pageSize: 1
                  })
              ).then(function (measurements) {
                  console.log("load measurement %s", measurements.length);
                   console.log("latest Mesurement %s",JSON.stringify(measurements));

                  //for (var i = 10; i < measurements.length; i++) {
                  var item = measurements[0];
                  $scope.powerMeasurement.push([Date.parse(item.time) + timezoneOffset, item.gdlk_smartLedMeasurement.Power.value]);
                  $scope.currentMeasurement.push([Date.parse(item.time) + timezoneOffset, item.gdlk_smartLedMeasurement.Current.value]);
                  $scope.voltageMeasurement.push([Date.parse(item.time) + timezoneOffset, item.gdlk_smartLedMeasurement.Voltage.value]);
                // }

              });

          };

          var updateControl = function() {
              c8yDeviceControl.list().then(function (operations) {
                 _.forEach(operations, function (operation) {
                      if (operation.deviceName == "gdlkSmartLedMasterCtrl") {
                          $scope.mainOperation = operation;
                          //console.log("mainOps", JSON.stringify(operation));
                          readPWMOperation();
                          //showPWM();
                      }

                  });

              });
          };

          var readPWMOperation = function(){
              var c_date = new Date();
              var c_time = c_date.getHours(c_date)*60 + c_date.getMinutes(c_date);
              var opsList =$scope.mainOperation.com_goldenpool_model_smartDevice.parameters;

              var timezoneOffset = 28800000;
              var c_pwm = [];

              $scope.pwmSeries= [];
              for(var i=1;i<6;i++){
                  var ctrllable = "ctrl"+i;

                      var tm = c_date.setHours(opsList[ctrllable].TM.H, opsList[ctrllable].TM.M, 0,0);
                      c_pwm[i] = opsList[ctrllable].PWM;
                      if(i>1){
                          $scope.pwmSeries.push([tm+timezoneOffset-10000,c_pwm[i-1]]);
                          $scope.pwmSeries.push([tm+timezoneOffset,c_pwm[i]]);
                      }else{
                          $scope.pwmSeries.push([tm+timezoneOffset,c_pwm[i]]);
                      }
              }

              $scope.pwmSeries.push([new Date().setHours(23,59,59,0)+timezoneOffset,c_pwm[5]]);

          };
      }

      var showPWM = function(){
          $scope.highchartsNG.series[1].data = $scope.pwmSeries;
          $scope.highchartsNG.series[1].name = 'PWM Setting';
          $scope.highchartsNG.series[1].yAxis = 1;
          $scope.highchartsNG.yAxis.title = 'PWM';
          $scope.highchartsNG.yAxis[1].max=100;
          $scope.highchartsNG.yAxis[1].min=0;

      };

      $scope.showMeasurement = function () {
          this.highchartsNG.series[0].yAxis = 0;

          if($scope.swapShow == 'Power'){
              $scope.swapShow = 'Current';
              this.highchartsNG.series[0].name = '漏电';
              $scope.highchartsNG.title.text = "线路漏电";
              this.highchartsNG.yAxis[0].max = 1.0;
              this.highchartsNG.yAxis[0].min = 0;
              $scope.highchartsNG.series[0].data = $scope.currentMeasurement;
              $scope.highchartsNG.series.splice(1,1);
          }
          else if($scope.swapShow == 'Current'){
              $scope.swapShow = 'Voltage';
              this.highchartsNG.series[0].name = '电压';
              $scope.highchartsNG.title.text = "线路电压";
              this.highchartsNG.yAxis[0].max = 250;
              this.highchartsNG.yAxis[0].min = 0;
              $scope.highchartsNG.series[0].data = $scope.voltageMeasurement;
              $scope.highchartsNG.series.splice(1,1);

          }
          else if($scope.swapShow == 'Voltage'){
              $scope.swapShow = 'Power';
              this.highchartsNG.series[0].name = '功率';
              $scope.highchartsNG.title.text = "功率 调光";
              this.highchartsNG.yAxis[0].max = 180;
              this.highchartsNG.yAxis[0].min = 0;
              $scope.highchartsNG.series[0].data = $scope.powerMeasurement;
              //series[1]
              //$scope.highchartsNG.series[1].data = $scope.pwmSeries;
              $scope.highchartsNG.series.push({data:$scope.pwmSeries});
              $scope.highchartsNG.series[1].name = 'PWM Setting';
              $scope.highchartsNG.series[1].yAxis = 1;
              $scope.highchartsNG.yAxis.title = 'PWM';
              $scope.highchartsNG.yAxis[1].max=100;
              $scope.highchartsNG.yAxis[1].min=0;
          }
      };

      (function(){
          updateControl();
          updateMeasurement();
      })();

      var repeatLoading = $interval(function(){
          updateMeasurement();
          updateControl();
          if($scope.swapShow == 'Power'){
              $scope.highchartsNG.series.splice(0,2);

              $scope.highchartsNG.series.push({data:$scope.powerMeasurement});
              $scope.highchartsNG.series[0].name = '功率';
              $scope.highchartsNG.yAxis[0].max = 180;
              $scope.highchartsNG.yAxis[0].min = 0;
              //series[1]
              //$scope.highchartsNG.series[1].data = $scope.pwmSeries;
              $scope.highchartsNG.series.push({data:$scope.pwmSeries});
              $scope.highchartsNG.series[1].name = 'PWM Setting';
              $scope.highchartsNG.series[1].yAxis = 1;
              $scope.highchartsNG.yAxis.title = 'PWM';
              $scope.highchartsNG.yAxis[1].max=100;
              $scope.highchartsNG.yAxis[1].min=0;


          }
          else if($scope.swapShow == 'Current'){
              $scope.highchartsNG.series.splice(0,1);
              $scope.highchartsNG.series.push({data:$scope.currentMeasurement});
              $scope.highchartsNG.series[0].name = '漏电';
          }
          else if($scope.swapShow == 'Voltage'){
              $scope.highchartsNG.series.splice(0,1);
              $scope.highchartsNG.series.push({data:$scope.voltageMeasurement});
              $scope.highchartsNG.series[0].name = '电压';
          }
      }, 60000);

      $scope.$on('$destroy', function(){
          $interval.cancel(repeatLoading);
          console.log("cancel repeatLoading()");
      })
  }

})();
