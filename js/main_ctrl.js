(function() {
    'use strict';

    angular.module('basic').controller('MainCtrl', [
        '$location',
        '$routeParams',
        'c8yUser',
        'c8yAlarms',
        '$echarts',
        '$timeout',
        '$scope',
        MainCtrl
    ]);
    /**
     * main页的controller
     * @param {object} $location    location的封装
     * @param {object} $routeParams 共享参数模块
     * @param {object} c8yUser      c8y用户模块
     * @param {object} c8yAlarms    c8y报警模块
     * @param {object} $echarts     echarts的封装
     * @param {object} $timeout     timeout的封装
     * @param {object} $scope       作用域的封装
     */
    function MainCtrl(
        $location,
        $routeParams,
        c8yUser,
        c8yAlarms,
        $echarts,
        $timeout,
        $scope
    ) {
        //验证用户登录
        c8yUser.current().catch(function() {
            $location.path('/login');
        });
        //路由纠错
        if ($routeParams.deviceId == undefined) {
            if (!$routeParams.section) {
                $location.path('/');
            }

        }

        this.currentSection = $routeParams.section;
        this.deviceId = $routeParams.deviceId;
        this.sections = {
            t1: '报警列表',
            t2: '设备列表',
            t3: '操作规则',
            t4: '统计分析',
            t5: '系统设置',
            t6: '用户交流'
        };
        this.filter = {};
        //注销事件
        this.logout = function() {
            $location.path('/login');
        };

        // $scope.alarmData = [];
        // c8yAlarms.list({status:"ACTIVE"}).then(function(alarms){
        //   $scope.alarmData = alarms;
        // })
        /**
         * 重置view
         * @param {string} pageId pageid
         */
        $scope.resetView = function(pageId){
          $scope.$broadcast("resetView",pageId);
        }
    }

})();
