(function () {
    'use strict';

    angular.module('basic').controller('AnotherCtrl', [
        '$scope',
        '$routeParams',
        '$route',
        'c8yBase',
        'c8yCounter',
        '$q',
        '$http',
        'c8yUser',
        'c8yInventory',
        '$cacheFactory',
        'c8yDevices',
        'c8yAlarms',
        'PublicData',
        'c8yGroupTypesConfig',
        'c8yGroups',
        AnotherCtrl
    ]);

    function AnotherCtrl($scope, $routeParams, $route, c8yBase, c8yCounter, $q, $http, c8yUser, c8yInventory, $cacheFactory, c8yDevices, c8yAlarms, PublicData, c8yGroupTypesConfig, c8yGroups) {
        var vm = this;
        vm.data = {
            message: "basic-another"
        };
        vm.data.deviceId4SingleCtrl = PublicData.getInfo("deviceId4SingleCtrl");
        $scope.$on("resetView",function(d,data){
          //d是当前事件
          if(data=='t2'){
            PublicData.delInfo("deviceId4SingleCtrl");
            c8yInventory.list(filters).then(function (devices) {
              $scope.devices = [];
              _.forEach(devices, function (device) {
                  $scope.devices.push(device);
                  // $scope.$apply("devices")
              });
            })
          }
        })
        $scope.smart =
            [
                {smartPower: "disabled", power: 'disabled'},
                {smartPower: "0", power: 0},
                {smartPower: "10", power: 10},
                {smartPower: "20", power: 20},
                {smartPower: "30", power: 30},
                {smartPower: "40", power: 40},
                {smartPower: "50", power: 50},
                {smartPower: "60", power: 60},
                {smartPower: "70", power: 70},
                {smartPower: "80", power: 80},
                {smartPower: "90", power: 90},
                {smartPower: "100", power: 100}
            ];

        var filters = {fragmentType: 'c8y_IsDevice', withParents: true};
        c8yInventory.list(filters).then(function (devices) {
            $scope.devices = [];
            _.forEach(devices, function (device) {
               if(vm.data.deviceId4SingleCtrl){
                    if(device.id==vm.data.deviceId4SingleCtrl){
                        $scope.devices.push(device);
                    }

                }else {
                    $scope.devices.push(device);
                }
                // console.log($scope.devices.com_gdlk_smartLED_PWMcontrol_setting);
            });

            // angular.forEach($scope.devices,function (res) {
            //     $scope.w = res.com_gdlk_smartLED_PWMcontrol_setting.PWM1;
            //
            //     if($scope.w==20&&res.com_gdlk_smartLED_PWMcontrol_setting.Mode == 888){
            //         console.log(1);
            //     }else if($scope.w==20&&res.com_gdlk_smartLED_PWMcontrol_setting.Mode == 888){
            //
            //     }
            // });
            // console.log($scope.devices);
        });



        $scope.deviceFn = function (data, id,assetId) {
            $scope.power = data.power;

            // c8yInventory.detail(id).then(function (res) {
            //     $scope.managedObject = res.data;
            //     console.log($scope.managedObject.com_gdlk_smartLED_PWMcontrol_setting);
            // });

            if ($scope.power == 'disabled') {

                //console.log(assetId);
                c8yInventory.parentsAsset(assetId).then(function (parents) {
                    $scope.parents = parents;

                    // console.log($scope.parents[0].com_gdlk_smartLED_PWMcontrol_setting);
                    // console.log($scope.parents[1].com_gdlk_smartLED_PWMcontrol_setting);
                    // console.log($scope.parents[2].com_gdlk_smartLED_PWMcontrol_setting);
                    if($scope.parents[0].com_gdlk_smartLED_PWMcontrol_setting!=''){
                        c8yInventory.save({
                            id: id,
                            com_gdlk_smartLED_PWMcontrol_setting:$scope.parents[0].com_gdlk_smartLED_PWMcontrol_setting
                        });
                        console.log(1);
                        return;
                    }else {
                        if($scope.parents[1].com_gdlk_smartLED_PWMcontrol_setting!=''){
                            c8yInventory.save({
                                id: id,
                                com_gdlk_smartLED_PWMcontrol_setting:$scope.parents[1].com_gdlk_smartLED_PWMcontrol_setting
                            });
                            console.log(2);
                            return;
                        }else{
                            if($scope.parents[2].com_gdlk_smartLED_PWMcontrol_setting!=''){
                                c8yInventory.save({
                                    id: id,
                                    com_gdlk_smartLED_PWMcontrol_setting:$scope.parents[2].com_gdlk_smartLED_PWMcontrol_setting
                                });
                                console.log(3);
                                return;
                            }else{
                                if($scope.parents[3].com_gdlk_smartLED_PWMcontrol_setting!='') {
                                    c8yInventory.save({
                                        id: id,
                                        com_gdlk_smartLED_PWMcontrol_setting: $scope.parents[3].com_gdlk_smartLED_PWMcontrol_setting
                                    });
                                    console.log(4);
                                    return;
                                }
                            }
                        }
                    }
                });

                // c8yInventory.detail(id).then(function (res) {
                //     $scope.managedObject = res.data;
                //     // console.log($scope.managedObject.com_gdlk_smartLED_PWMcontrol_setting);
                // });

            } else{

                c8yInventory.save({
                    id: id,
                    com_gdlk_smartLED_PWMcontrol_setting: {
                        "PWM4": $scope.power,
                        "PWM2": $scope.power,
                        "PWM3": $scope.power,
                        "PWM1": $scope.power,
                        "Mode": 888,
                        "T1": 0,
                        "T3": 0,
                        "T2": 0
                    }
                });

                // c8yInventory.detail(id).then(function (res) {
                //     $scope.managedObject = res.data;
                //     // console.log($scope.managedObject.com_gdlk_smartLED_PWMcontrol_setting);
                // });

            }


            // c8yInventory.detail(id).then(function (res) {
            //     $scope.managedObject = res.data;
            //     console.log($scope.managedObject.com_gdlk_smartLED_PWMcontrol_setting);
            // });
        };



        $scope.currentPage = 1;
        $scope.pageSize = 5;
        $scope.pl = false;
        $scope.pl2 = false;

        $scope.alarm = function () {
            if ($scope.pl == false) {
                $scope.currentPage++;
            }

            // console.log($scope.currentPage);
            c8yAlarms.list({
                // source: deviceId,
                // severity: c8yAlarms.severity.MAJOR,
                // status: c8yAlarms.status.ACTIVE,
                // resolved: false,
                currentPage: $scope.currentPage,
                pageSize: $scope.pageSize
            }).then(function (alarms) {
                $scope.alarms = [];
                _.forEach(alarms, function (alarm) {
                    $scope.alarms.push(alarm);
                });
                // console.log($scope.alarms);
                if ($scope.alarms == "") {
                    $scope.pl = true;
                } else {
                    $scope.pl = false;
                    $scope.pl2 = false;
                }

            });
        };

        $scope.alarmFn = function () {

            $scope.currentPage--;

            // console.log($scope.currentPage);
            c8yAlarms.list({
                // source: deviceId,
                // severity: c8yAlarms.severity.MAJOR,
                // status: c8yAlarms.status.ACTIVE,
                // resolved: false,
                currentPage: $scope.currentPage,
                pageSize: $scope.pageSize
            }).then(function (alarms) {
                $scope.alarms = [];
                _.forEach(alarms, function (alarm) {
                    $scope.alarms.push(alarm);
                });
                if ($scope.alarms != '') {
                    $scope.pl = false;
                }
                // console.log($scope.alarms);
                if ($scope.currentPage <= 1) {
                    $scope.pl2 = true;
                    $scope.currentPage = 1;
                } else {
                    $scope.pl2 = false;
                }
            });
        };


        c8yAlarms.list({
            // source: deviceId,
            // severity: c8yAlarms.severity.MAJOR,
            // status: c8yAlarms.status.ACTIVE,
            // resolved: false,
            // currentPage:($scope.page-1)*$scope.pageSize,
            // pageSize: $scope.pageSize
            // q:$routeParams.q
        }).then(function (alarms) {
            $scope.alarms = [];
            _.forEach(alarms, function (alarm) {
                // console.log(alarm.status);
                if (alarm.status == 'ACTIVE') {
                    $scope.alarms.push(alarm);
                }
            });
            // console.log($scope.alarms);
        });


        $scope.particular = function (id) {
            c8yAlarms.detail(id).then(function (alarm) {
                $scope.alarmData = alarm.data;
                // console.log($scope.alarm);
                if ($scope.alarmData.severity == 'WARNING') {
                    $scope.alarmText = "警告";
                } else if ($scope.alarmData.severity == 'MINOR') {
                    $scope.alarmText = "次要";
                } else if ($scope.alarmData.severity == 'MAJOR') {
                    $scope.alarmText = "主要";
                } else if ($scope.alarmData.severity == 'CRITICAL') {
                    $scope.alarmText = "严重";
                }

                if ($scope.alarmData.status != "CLEARED") {
                    $scope.statusText = "未处理";
                } else {
                    $scope.statusText = "已处理";
                }
            });
        };

        $scope.handle = function (id) {
            c8yAlarms.save({
                id: id,
                status: c8yAlarms.status.CLEARED
            });

            $(".modal-backdrop").fadeOut();

            $route.reload();

            $scope.statusText = "已处理";
        };


    }

})();
