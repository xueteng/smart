(function() {
    'use strict';

    angular
        .module('basic')
        .service('PublicData', PublicData);

    // PublicData.$inject = [];
    /**
     * 数据共享模块service
     * @return {object}     PublicData对象模块
     */
    function PublicData() {
        var data = {};

        return {
            /**
             * 保存数据
             * @param  {string} key key
             * @param  {object} val value
             */
            saveInfo:function(key,val){
				data[key]=val
			},

            /**
             * 获取保存的数据
             * @param  {string} key key
             * @return {object}     对应的属性值
             */
			getInfo:function(key){
				if(data[key]){
					return data[key]
				}else{
					return undefined
				}
			},
            /**
             * 删除保存的数据
             * @param  {string} key key
             */
            delInfo:function(key){
                if(data[key]){
                    delete data[key]
                }
            }
        }
    }
})();
