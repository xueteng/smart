(function () {
  'use strict';

  angular.module('basic').controller('LoginCtrl', [
    '$location',
    'c8yUser',
    LoginCtrl
  ]);

  function LoginCtrl(
    $location,
    c8yUser
  ) {
    this.tenant = "smart1";
    c8yUser.current().then(function () {
      $location.path('/');
    });

    this.credentials = {};
    this.onSuccess = function () {
      $location.path('/');
    };
  }
})();
