/**
 * 废弃的section指令模块
 */
(function () {
  'use strict';

  angular.module('helloCoreApi').controller('SectionCtrl', ['$scope','$location',
       SectionCtrl
  ]).directive('egSection', [
    egSection
  ]);

  function SectionCtrl(
    $scope,$location
  ) {
    this.filter = $scope.filter || {};
    this.filter.pageSize = 10;
    this.service = $scope.service;
    this.deviceId = "";
    $scope.$watch('section.refresh', function (val) {
      $scope.refresh = val;

    });
    $scope.deviceDetail = function(id){
     var url = '/devices/'+id;
      $location.path(url);
      console.log(url);
  }
  }

  function egSection(
  ) {
    return {
      restrict: 'AE',
      templateUrl: 'views/section.html',
      controller: 'SectionCtrl',
      controllerAs: 'section',
      transclude: true,
      replace: true,
      scope: {
        service: '@',
        filter: '=?',
        refresh: '=?'
      }
    };
  }



})();
