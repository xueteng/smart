/**
 * 废弃的treeview模块
 */
(function () {
  'use strict';
  angular.module('basic')
  .controller('treeCtrl', ['$scope','c8yDevices','$http',treeCtrl]);

  function treeCtrl($scope,c8yDevices,$http) {
    $scope.treedata =[];
    $scope.tempData = {};
    c8yDevices.list({fragmentType:"",type:"c8y_DeviceGroup",skipChildrenNames:"false"}).then(function (devices) {
      $scope.devices = devices;
      angular.forEach($scope.devices,function(item){
          $scope.treedata.push(getTreeData(item))
          $scope.tempData[item.id] = item.self;
      })
    });
    $scope.$watch( 'abc.currentNode', function( newObj, oldObj ) {
        if( $scope.abc && angular.isObject($scope.abc.currentNode) ) {
            nodeAppendChild($scope.abc.currentNode.id,$scope.treedata)
        }
    }, false);
    function getTreeData(data) {
      return {
        "label":data.name+" "+data.childAssets.references.length,
        "id":data.id,
        "children":[]
      }
    }
    function nodeAppendChild(id,treedata){
        var url = $scope.tempData[id];
        $http.get(url).then(function(data){
            getNodeChildById(id,treedata,data.data)
        })
    }
    function getNodeChildById(id,treedata,responseData){
        for(var i=0;i<treedata.length;i++){
            if(treedata[i].id == id&&treedata[i].children.length==0){
                angular.forEach(responseData.childAssets.references,function(item){
                    treedata[i].children.push({
                        "label":item.managedObject.name,
                        "id":item.managedObject.id,
                        "children":[]
                    });
                    $scope.tempData[item.managedObject.id] = item.managedObject.self;
                });
                break;
            }else if (treedata[i].children.length>0) {
                getNodeChildById(id,treedata[i].children,responseData)
            }
        }
    }
    window.VM = $scope.tempData
    // $scope.treedata =
    // [
    //   { "label" : "用户1", "id" : "role1", "children" : [
    //     { "label" : "subUser1", "id" : "role11", "children" : [] },
    //     { "label" : "subUser2", "id" : "role12", "children" : [
    //       { "label" : "subUser2-1", "id" : "role121", "children" : [
    //         { "label" : "subUser2-1-1", "id" : "role1211", "children" : [] },
    //         { "label" : "subUser2-1-2", "id" : "role1212", "children" : [] }
    //       ]}
    //     ]}
    //   ]},
    //   { "label" : "用户2", "id" : "role2", "children" : [] },
    //   { "label" : "用户3", "id" : "role3", "children" : [] }
    // ];
  }
})();
