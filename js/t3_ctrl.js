(function() {
    'use strict';

    angular.module('basic')
        .controller('OperationCtrl', ["c8yDeviceControl", 'c8yDevices', 'c8yInventory', 'ngDialog', '$scope', 'PublicData', '$echarts', OperationCtrl])
        .controller('RuleEditCtrl', ["$scope", "c8yInventory", RuleEditCtrl]);

    /**
     * 规则页controller
     * @param {object} c8yDeviceControl c8y设备控制模块
     * @param {object} c8yDevices       c8y设备模块
     * @param {object} c8yInventory     c8y对象管理模块
     * @param {object} ngDialog         模态对话框模块
     * @param {object} $scope           作用域封装
     * @param {object} PublicData       共享数据模块
     * @param {object} $echarts         echarts封装
     */
    function OperationCtrl(c8yDeviceControl, c8yDevices, c8yInventory, ngDialog, $scope, PublicData, $echarts) {
        var vm = this;
        window.VM = $scope;
        vm.data = {
            operationId: "",
            deviceId: "",
            name: "",
            operationList: [],
            currentOperation: "",
            operations: ["一般规则", "雾霾天规则", "常亮规则", "演示规则", "默认规则"],
            operationsDetail: ["一般通用规则，包括4个功率值和3个变更时间点", "日照不良时规则，包括3个功率值和2个变更时间点", "特殊常亮规则，包括1个功率值和0个变更时间点", "演示用规则，由后台自动控制功率变化"],
            templateOperations: {
                "Mode1":{
                    "PWM1": 45,
                    "PWM2": 45,
                    "PWM3": 45,
                    "PWM4": 45,
                    "T1": 1200,
                    "T2": 1300,
                    "T3": 1400,
                    "Mode": 1
                },
                "Mode2":{
                    "PWM1": 45,
                    "PWM2": 45,
                    "PWM3": 45,
                    "PWM4": 45,
                    "T1": 1200,
                    "T2": 1300,
                    "T3": 1300,
                    "Mode": 2
                },
                "Mode3":{
                    "PWM1": 45,
                    "PWM2": 45,
                    "PWM3": 45,
                    "PWM4": 45,
                    "T1": 1440,
                    "T2": 1440,
                    "T3": 1440,
                    "Mode": 3
                },
                "Mode4":{
                    "PWM1": 0,
                    "PWM2": 0,
                    "PWM3": 0,
                    "PWM4": 0,
                    "T1": 0,
                    "T2": 0,
                    "T3": 0,
                    "Mode": 4
                }
            }
        };
        vm.data.deviceId4SingleCtrl = PublicData.getInfo("deviceId4OperationEdit");
        vm.fns = {};
        $scope.DISTRIBUTION_IDS = [
            $echarts.generateInstanceIdentity(),
            $echarts.generateInstanceIdentity(),
            $echarts.generateInstanceIdentity(),
            $echarts.generateInstanceIdentity()
        ];
        vm.distribution = {
            identity: $echarts.generateInstanceIdentity(),
            dimension: '16:9',
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['功率']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                data: ['0点', 'T1', 'T2', 'T3', '24点']
            },
            yAxis: {
                type: 'value'
            },
            series: [{
                name: '功率',
                type: 'line',
                step: 'end',
                data: [450, 432, 401, 454]
            }]
        };
        $scope.distributions = [
            angular.copy(vm.distribution),
            angular.copy(vm.distribution),
            angular.copy(vm.distribution),
            angular.copy(vm.distribution)
        ];

        /**
         * 保存规则到设备
         * @param  {string} index 根群组的id，递归的起点对象id
         */
        vm.fns.save = function(index) {
            console.log(vm.data.operationList);
            //将结果保存到PublicData
            PublicData.saveInfo("savedIndex", index);
            //为当前群组保存规则
            singleSave(vm.data.deviceId4SingleCtrl, vm.data.operationList, index);
            //递归保存
            c8yInventory.childAssets(vm.data.deviceId4SingleCtrl).then(function(data) {
                recurseSave(data, vm.data.operationList, index);
                // delFragment(data);
            })
        };
        /**
         * 获取设备规则以编辑
         * @param  {string} id    目标群组/设备id
         * @param  {string} index operationList中被修改项的key
         */
        vm.fns.edit = function(id, index) {
            console.log(index)
            // c8yAlert.success("您正在修改"+id+"号规则")
            // alert("您正在修改"+id+"号规则")
            ngDialog.open({
                template: 'popupTmpl.html',
                className: 'ngdialog-theme-default',
                controllerAs: 'svm',
                controller: [
                    '$scope',
                    'c8yInventory',
                    '$echarts',
                    RuleEditCtrl
                ],
                data: {
                    data: vm.data.operationList["Mode"+id],
                    operations: vm.data.operationList,
                    // id: vm.data.operationList[index].id,
                    deviceId: vm.data.deviceId4SingleCtrl,
                    index: index
                },
                scope: $scope
            });
        }
        /**
         * 删除规则
         */
        vm.fns.delRule = function() {
            console.log(vm.data.operationList);
            //获取可用的父级规则
            c8yInventory.parentsAsset(vm.data.deviceId4SingleCtrl).then(function(res) {
                console.log(res);
                if (res.length > 0) {
                    PublicData.saveInfo("savedIndex", res[0].com_gdlk_smartLED_PWMcontrol_setting.Mode);
                    // 为当前群组保存规则
                    singleSave(vm.data.deviceId4SingleCtrl, res[0].PWMcontrolList, res[0].com_gdlk_smartLED_PWMcontrol_setting.Mode);
                    //递归保存
                    c8yInventory.childAssets(vm.data.deviceId4SingleCtrl).then(function(data) {
                        recurseSave(data, res[0].PWMcontrolList, res[0].com_gdlk_smartLED_PWMcontrol_setting.Mode);
                        // delFragment(data);
                    })
                } else {
                    alert("没有可用的父级规则")
                }
            })

        }
        start();
        //启动
        function start() {
            if (vm.data.deviceId4SingleCtrl) {
                c8yDevices.detail(PublicData.getInfo("deviceId4OperationEdit"), {
                        "withParents": true
                    })
                    .then(function(data) {
                        vm.data.name = data.data.name;
                        vm.data.currentOperation = data.data.com_gdlk_smartLED_PWMcontrol_setting && data.data.com_gdlk_smartLED_PWMcontrol_setting.Mode || 5;
                        //如果已被分配规则
                        if (data.data.com_gdlk_smartLED_PWMcontrol_setting && data.data.com_gdlk_smartLED_PWMcontrol_setting.Mode > 0) {
                            vm.data.operationList = data.data.PWMcontrolList || vm.data.templateOperations;
                            chartsDataFormat(vm.data.operationList, $scope.distributions);

                        } else {
                            //get parent group's operation

                            //if has parent
                            if (data.data.assetParents.references.length > 0) {
                                var parent = data.data.assetParents.references[0];
                                var parentId = parent.managedObject.id;
                                //vm.data.operationList赋值
                                c8yDevices.detail(parentId).then(function(data) {
                                    vm.data.currentOperation = data.data.com_gdlk_smartLED_PWMcontrol_setting.Mode || 4;
                                    vm.data.operationList = data.data.PWMcontrolList || vm.data.templateOperations;
                                    //TODO:递归指派规则
                                    chartsDataFormat(vm.data.operationList, $scope.distributions);

                                })
                            }
                            //else,use templateOperations
                            else {
                                vm.data.operationList = vm.data.templateOperations
                                chartsDataFormat(vm.data.operationList, $scope.distributions);

                                //TODO:递归指派规则
                            }
                        }
                    })
            } else {
                // vm.data.name = "全国";
                // vm.data.currentOperation = 4;
                // c8yInventory.list({"type":"c8y_DeviceGroup"})
                // .then()
            }
        }
        /**
         * 调取接口保存规则
         * @param  {string} id         操作对象的id
         * @param  {object} operations 规则列表
         * @param  {string} chosen     列表中被选中的属性的序号
         * @param  {object} type       操作对象的type，isDevice或false
         */
        function singleSave(id, operations, chosen, type) {
            if (type) {
                c8yInventory.save({
                    id: id,
                    com_gdlk_smartLED_PWMcontrol_setting: deepCopy(operations["Mode"+chosen]),
                    PWMcontrolList: null
                })
            } else {
                c8yInventory.save({
                    id: id,
                    com_gdlk_smartLED_PWMcontrol_setting: deepCopy(operations["Mode"+chosen]),
                    PWMcontrolList: formatList(deepCopy(operations))
                })

            }
        }
        /**
         * 递归调用规则保存
         * @param  {array} arr        操作对象的数组
         * @param  {object} operations 规则列表
         * @param  {string} chosen      列表中被选中的属性的序号
         */
        function recurseSave(arr, operations, chosen) {
            if (arr.length > 0) {
                angular.forEach(arr, function(item) {
                    singleSave(item.id, operations, chosen, item.c8y_IsDevice);
                    if (!item.c8y_IsDevice) {
                        c8yInventory.childAssets(item.id).then(function(data) {
                            recurseSave(data, operations, chosen)
                        })
                    } else {
                        console.log("ok");
                    }
                })
            } else {
                console.log("ok");
            }
        }
        /**
         * 递归调用删除属性方法，删除无用的属性
         * @param  {array} arr 操作对象的数组
         */
        function delFragment(arr) {
            if (arr.length > 0) {
                angular.forEach(arr, function(item) {
                    singDel(item.id);
                    if (!item.c8y_IsDevice) {
                        c8yInventory.childAssets(item.id).then(function(data) {
                            delFragment(data)
                        })
                    } else {
                        console.log("del" + item.id)
                    }
                })
            } else {
                console.log("end");
            }
        }
        /**
         * 删除无用的属性
         * @param  {string} id 目标id
         */
        function singDel(id) {
            c8yInventory.save({
                id: id,
                "new fragment": null
            })
        }

    }
    /**
     * 规则编辑模态对话框的controller
     * @param {object} $scope       公共作用域
     * @param {object} c8yInventory c8y对象模块
     * @param {object} $echarts     echarts的封装
     */
    function RuleEditCtrl($scope, c8yInventory, $echarts) {
        //从哪个DialogData获取用于编辑的规则
        $scope.newOperation = $scope.ngDialogData;
        // $scope.log = function() {
        //     console.log($scope.newOperation)
        // };
        /**
         * 保存规则
         * @param  {string} id    目标id
         * @param  {string} index 选中的序号
         */
        $scope.saveNewOperation = function(id, index) {
            var editedOperation = deepCopy($scope.ngDialogData.operations);
            angular.forEach(editedOperation, function(item) {
                switch (item.Mode) {
                    case 2:
                        item.PWM4 = item.PWM3;
                        item.T3 = item.T2;
                        break;
                    case 3:
                        item.PWM2 = item.PWM1;
                        item.PWM3 = item.PWM1;
                        item.PWM4 = item.PWM1;
                        item.T1 = 1440;
                        item.T2 = 1440;
                        item.T3 = 1440;
                        break;
                    case 4:
                        item.T1 = 0;
                        item.T2 = 0;
                        item.T3 = 0;
                        item.PWM1 = 0;
                        item.PWM2 = 0;
                        item.PWM3 = 0;
                        item.PWM4 = 0;
                        break;
                    default:

                }
            });
            chartsDataFormat(editedOperation, $scope.distributions);
            $echarts.updateEchartsInstance($scope.DISTRIBUTION_IDS[index], $scope.distributions[index]);
            c8yInventory.save({
                id: $scope.ngDialogData.deviceId,
                PWMcontrolList: editedOperation
            })
            $scope.closeThisDialog(id);
        }
    }
    /**
     * 深拷贝实现，在拷贝时对string型value做修改，转为number型
     * @param  {[type]} source [description]
     * @return {[type]}        [description]
     */
    function deepCopy(source) {
        var result = {};
        for (var key in source) {
            result[key] = typeof source[key] === 'object' ? deepCopy(source[key]) : parseInt(source[key]);
        }
        return result;
    }
    /**
     * 格式化echarts图表需要的数据源
     * @param  {object} source 数据源，规则列表
     * @param  {array} target 目标，需要被格式化的数据
     */
    function chartsDataFormat(source, target) {
        angular.forEach(target, function(item, index) {
            item.series[0].data[0] = source["Mode"+(index+1)].PWM1;
            item.series[0].data[1] = source["Mode"+(index+1)].PWM2;
            item.series[0].data[2] = source["Mode"+(index+1)].PWM3;
            item.series[0].data[3] = source["Mode"+(index+1)].PWM4;
            item.series[0].data[4] = source["Mode"+(index+1)].PWM4;
        })
    }
    /**
     * 规则列表由{"0":value}转化为{"ModeN":value}
     * @param  {object} obj 规则列表
     */
    function formatList(obj) {
        if(obj[0]){
            return {
                "Mode1":obj[0],
                "Mode2":obj[1],
                "Mode3":obj[2],
                "Mode4":obj[3]
            }
        }else {
            return obj
        }
    }
})();
